---
layout: markdown_page
title: "Tiering Working Group"
description: ""
canonical_path: "/company/team/structure/working-groups/tiering/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes
 
| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 29, 2020   |
| Target End Date |   |
| Slack           | [#wg_tiering]() (only accessible from within the company) |
| Google Doc      | [Tiering Working Group Agenda](https://docs.google.com/document/d/1fYIvcYpAkcreWZOfz01Wxp5QkD_NG8P1NOlQpq2TXxc/edit#heading=h.qtqrv8ra975g) (only accessible from within the company) |


## Business Goal

Leverage team members from other groups to start working on tiering projects so that we can accelerate the delivery of the work. 

## Roles and Responsibilities

| Working Group Role              | Person                | Title                                                        |
|---------------------------------|-----------------------|--------------------------------------------------------------|
| Facilitator                     | Justin Farris        |  Group Manager, Product - Fulfillment |
| Executive Stakeholder | Sid Sijbrandij          | CEO                          |
| Executive Stakeholder | Michael McBride           | CRO    |
| Member         | Xiaohe Li   | Principal Pricing Manager            |
| Member         | Bartek Marnane   | Director of Engineering, Growth            |
| Member         | Jack Brennan   | Director, Sales Systems            |
| Member         | Amanda Rueda   | Product Manager, Fulfillment          |
| Member         | Dale Brown   | Principal Accounting Officer            |
| Member         | Christopher Lefelhocz   | VP of Development          |
| Member         | Scott Williamson  | EVP of Product        |


                                         

## Exit Criteria

The Tiering Working Group will deliver:
* [ ] Execute internal transfers
* [ ] Onboard transferred team members
* [ ] Agree on the scope for the announcement
* [ ] Confirm target announcement date



