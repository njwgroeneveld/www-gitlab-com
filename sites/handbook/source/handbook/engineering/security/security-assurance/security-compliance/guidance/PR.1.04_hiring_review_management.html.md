---
layout: handbook-page-toc
title: "PR.1.04 - Hiring Review by Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# PR.1.04 - Hiring Review by Management

## Control Statement
Hiring candidates are reviewed by People Ops and approved by management before an employment position is offered.

## Context
We need to demonstrate that candidates are evaluated for competency and address potential shortcomings prior to extending any employment offers.

## Scope
This applies to all GitLab, Inc. employees


## Ownership
* PeopleOps: `100%`  


## Guidance
In order to ensure candidates are reviewed, there should be a process for internal feedback as a step before finalizing hiring decisions.

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Policy and Procedure Review control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/1707).

### Policy Reference
* [Recruiting Hiring Manager Process](/handbook/hiring/recruiting-framework/hiring-manager/)
* [Recruiting Coordinator Process](/handbook/hiring/recruiting-framework/coordinator/)
* [Greenhouse Handbook page](/handbook/hiring/greenhouse/)

## Framework Mapping
* SOC
  * CC1.4
