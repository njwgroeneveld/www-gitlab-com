---
layout: handbook-page-toc
title: "PathFactory"
description: "PathFactory is an intelligent B2B content experience platform used to create personalized content journeys for your audience."
"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### What is PathFactory?

PathFactory is an intelligent B2B content experience platform used to create personalized content journeys for your audience. The [global content](/handbook/marketing/growth-marketing/content/) team is primarily responsible for all the of the content within the PathFactory. Marketing campaign managers work with other teams in marketing to organize and curate assets into tracks that are then disseminated for use in Marketo, `about.gitlab.com`, and other campaign-related channels.

#### PathFactory for Sales

[PathFactory for Sales](/handbook/marketing/marketing-operations/pathfactory/pathfactory-for-sales) allows B2B sales teams to deliver and track personalized content to engage buyers and close deals. 

#### PathFactory vs. Marketo

☝️ **PathFactory ≠ Email Nurture. PathFactory is a tool that - instead of driving to a single asset - drives to a related-content experience.**

Nurture is a channel to bring an individual to the content. Just like ads, social, website, etc. drive to CTAs, PathFactory link is the CTA - a _powerful_ CTA because it can lead the individual down a "choose your own adventure" path of consumption which we track.

**What are the differing goals of PathFactory and Marketo?**

- **Marketo nurture:** To keep the GitLab top of mind and deliver relevant content (via PathFactory)
- **PathFactory:** To increase consumption/engagemet with GitLab content

**Can records be in Marketo nurture and PathFactory nurture at the same time? If not, is 1 prioritized over the other?**

Yes, the PathFactory track acts as a supplement to the existing Marketo nurture instead of a replacement. It allows us to provide related content in a seamless way for the end-user (better than simply providing a PDF link and to improve "binging" of content).

#### PathFactory vs. CMS vs. DAM

PathFactory is not a single source of truth (SSoT) for all GitLab content, but rather content that has been activated in a marketing campaign.

**Content Management System (CMS)** - A content management system is a software application that can be used to manage the creation and modification of digital content.

**Digital Asset Management (DAM)** - Digital asset management (DAM) is a system that stores, shares and organizes digital assets in a central location.

**PathFactory** - Content experience software used to allow buyers to binge-consume content, remove friction, and create more qualified leads, faster.

### Access

The Marketing Operations team is responsible for managing access requests & provisioning the appropriate level of access for each role/function. PathFactory is not provisioned as a baseline entitlement. If you or your team require access, please open a [`Single Person Access Request` issue](/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) and provide business reason why access is needed. For certain permissions or roles, you may be required to go through training before access is provisioned.

### User Roles

There are three levels of access - `Admin`, `Author`, `Reporter` - which are granted based on the persons' role and team function within GitLab. All access levels have the ability to view analtyics within the tool. 

- `Admin` access is granted to Marketing Operations and at times, other teams such as sales or business operations to manage integrations between systems.
- `Author` access allows user to build, edit and publish content tracks applying existing tags to the assets.
- `Reporter` access provides general visibility to content within PathFactory but does not allow end user to create or modify any of the content, tracks or tags. This level of access is granted for the general GitLab team member both within Marketing and elsewhere who have a business need to have access. We have an unlimited amount of `Reporter` seats available for provisioning.

For more info on the capabilities of each role, [see the knowledge base article](https://nook.pathfactory.com/nook/s/article/user-roles). 

### Rules of Engagement by Team

| Team | User Roles | Rules of Engagement |
| ---- | ---------- | ------------------- |
| Marketing Operations | `Admin` | Manage, quality assurance, user management, system integrations, training |
| Marketing Campaign Managers | `Author` | Add content, create and edit content tracks for use in campaigns |
| Content Marketing | `Author` | Upload new content, quality assurance |
| Field Marketing | `Reporter` | View content performance |
| Account Based Marketing | `Author` | Add content, create and edit content tracks for use in campaigns |
| Customer Reference Programs | `Author` | Upload new case studies and customer content |
| Developer Evangelism | `Author` | Upload new technical content |
| Sales Development Representative (SDR) | `Sales user` (PathFactory for Sales only) | PathFactory for Sales (SFDC) |
| All Remote | `Author` | Upload new all remote content, create and edit content tracks for use in all-remote campaigns |

## Support

1. [Knowledge base, _The Nook_](https://lookbookhq.force.com/nook/s/kb) (requires user in PathFactory and the email you use for PathFactory will be the same to access The Nook, however, the password is not.)
1. [Success Series Webinars](https://customer.pathfactory.com/l/success-series-recordings)
1. [PathFactory Ideas Portal](https://ideas.pathfactory.com/portal_session/new)
1. [Release Notes](https://nook.pathfactory.com/nook/s/module?m=release-notes) (requires access to _The Nook_)

## Training

1. [The Path to Understanding: Onboarding Training](https://lp.pathfactory.com/PathFactory-Onboarding.html)
1. [Getting started video series](http://successwith.pathfactory.com/c/lookbookhq-tutorial-?x=Blrk3E)
1. [Digital marketing brown bag session overview](https://drive.google.com/open?id=1Hzb6ard48k-11r5a8oBDD_NLjeZnkMK2) - [Slides](https://drive.google.com/open?id=1XxOIE2O-VW0I9z09kpLs5ops52oF6iDSP1a1MF8NkGY)
1. [Author role training (Do not share externally - PII data presented)](https://drive.google.com/file/d/1YdK96hzDj043iESfDXV7ejz5sgbIXKCv/view?usp=sharing)
1. [Reporter role training (Do not share externally - PII data presented)](https://drive.google.com/file/d/1U_QAkZoELITmJt7Jr_AMXZiQZBpAhaIj/view?usp=sharing)

### Issue templates

**[Marketing Operations](https://about.gitlab.com/handbook/marketing/marketing-operations/)**

1. [Generic PathFactory request](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/pathfactory_request.md)

**[Campaigns](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/)**

1. [Pathfactory Upload Request](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload)
1. [Pathfactory Bulk Upload Request](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload-bulk)
1. [Pathfactory Track Request](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track)

### Changelog

Periodically, significant changes are made or added to PathFactory and processes that affect overall data and reporting. As such we have a [changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit?usp=sharing). Everyone with `Author` access that is making significant changes in PathFactory should add their changes to the changelog with a linked issue and/or relevant PathFactory links.

**Scenarios for adding to the changelog:**

1. Creation or launch of a new content track and where it will be used
1. Creation or launch of a new website promoter
1. Additions or changes to a form strategy within a `[LIVE]` content track
1. Additions or changes to assets within a `[LIVE]` content track
1. Expiration of an asset from the content library
1. Change of a custom URL slug for an asset or content track and why
1. Change of promote settings within a `[LIVE]` content track and why

**Instructions**

1. Open the [PathFactory changelog doc](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit?usp=sharing).
1. Add the date you made the change if it doesn't already exist.
1. Add a bullet with your GitLab username and document the changes you made, making sure to include links to issues or relevant PathFactory links.

## Content Library

The [content library](/handbook/marketing/marketing-operations/pathfactory/content-library/) is where all content uploaded to PathFactory is added and tagged for use in content tracks and experiences. 

## Content Tracks

Before creating a new content track, decide what type of content track (target vs. recommend) you want to create. Use the criteria below to decide the best option:

#### Best practices

**Target track**

1. Curated content
1. Known audience
1. Personalized journey (email, website, targeted display)
1. 5-7 pieces of content
1. Use target in webinar reg and follow-up
1. Use target to GUIDE

**Recommended track**

1. Dynamic content sequence (it will automatically move content to the top of the track that is performing well)
1. Anonymous audience
1. Personalized journey (web, general display, social)
1. Tracks the most popular journey (which pieces are being viewed, can be exported into a target track)
1. Use recommend to DISCOVER

#### Create a content track

1. When you're ready, select `Campaign Tools` in the top navigation bar and select the content track type you want to create (target or recommend).
1. Click the `Create track` button in the top right.
1. Name your content track. Be descriptive about the topic or campaign that your track will be used for. If you're unsure, review the names of other content tracks to get an idea. The content track name is for internal use only and will not be shown to visitors. Each content track has to have a unique name. You cannot use duplicate names for content tracks.
1. Choose to clone an existing content track, which will copy all assets from that content track into yours, or simply start from scratch.
1. Choose the folder where your content track will live. Follow the current folder hierarchy for organization which is currently set up by team.
1. Tag your content track with labels that will tell everyone accessing the content track which channels it will be used on (internal use only).
1. Click `Create Track`.
1. On the left panel, change the custom URL slug to be descriptive of the track with no stop words (and, the, etc.) - ex. `ci-aware`
1. Add assets to your track by clicking the `Add content` button at the top right. A window will pop up with the content library. Use the sorting options at the top to quickly add content by topic, type, funnel stage, etc. PathFactory content tracks are meant to encourage content binging (visitors reading more than 1 asset to accelerate them in the pipeline by helping them self educate faster in one visit). So as best practice, have more than 1 asset in a track.

##### Configuring content track settings

###### Track settings

1. Set the `custom URL slug` for the content track. [Follow the instructions](/handbook/marketing/marketing-operations/pathfactory/content-library/) for creating a custom URL slug for an asset. **Important:** All content tracks should be set up with custom URL slugs to prevent any future changes to the track from breaking the link and thus the user experience. If you change the custom URL slug after a PathFactory link has been implemeneted, those links have to be updated wherever they were used (ads, emails, website, etc.).
1. Ensure that the Search Engine Directive is set to `No Index, No Follow`.
1. Set the appearance for the track. 
1. Set the language for the track.
1. Leave `Links & Sharing` set to default.
1. Leave `External ID` set to default.
1. Turn on the `Cookie Consent` before providing the approved content track link for live use.

####### Promoters

This is where you will choose how your content track displays to the visitor. Note the different functionality of each below. Before going live, you can test each promoter to find the one that works best for the goals of your PathFactory experience.

- You can only use the `Header` feature with the `Sign Posts` and `Bottom Bar` promoters.
- The `Header` is used to add additional branding to your content track.
- The `Flow`, `Sign Posts`, and `Bottom Bar` cannot be used together. Choose 1 of the 3.
    - **Flow:** Scrollable content menu allows visitors to jump ahead in their Content Track, or simply use the Next buttons to move forward.
    - **Sign Posts:** Customizable Next and Previous buttons allow visitors to navigate through content. Used for more of a linear journey through the content.
    - **Bottom Bar:** Collapsible content menu along page bottom.
- The `End Promoter`, `Exit`, and `Inactivity` promoters can be used in conjunction with either the `Flow`, `Sign Posts`, or `Bottom Bar` promoters.
    - **End Promoter:** Opens final asset in a new tab.
        - Available overrides:
            - Link
            - CTA Label
            - Delay (seconds)
    - **Exit:** Suggested content window appears when visitor tries to navigate away from the Content Track.
        - Available overrides:
            - Headline
            - Message
            - Items to show (choose from assets within the current track)
            - Delay (seconds)
    - **Inactivity:** Message flashes on tab when left inactive.
        - Available overrides:
            - Inactive tab title
            - Delay (seconds)

###### CTAs

[Using CTA Buttons](https://nook.pathfactory.com/nook/s/article/using-cta)

**Create a CTA**

1. Click the settings (gear icon) and navigate to `CTAs` under `User Experience`. 
1. Click the `Add CTA` button. 
1. Enter a name for the CTA (internal purposes only). Be specific so others know what the main goal of the CTA is.
1. Enter a button label for your CTA. This is what the user will see. Copy for CTAs should typically be 2-3 words max (18 characters) and include action words such as `Watch a demo`.
1. Choose your CTA type (form, link, email). 
  - If choosing **form** as your CTA type, **it must include** proper capture tags and script. Do not choose a `WIP` form as your CTA.
  - If choosing **link** as your CTA type, **it must include** UTM paramters for proper tracking (ex: `utm_source=pathfactory&utm_medium=cta-name`)
  - For **email**, simply input the email address you want users to contact from clicking the CTA.
1. Click `Save`.

###### Form strategy

Form strategy is used on content tracks to collect data from unknown visitors by "gating" an asset within a track or by creating a track rule with an engagement threshold (example: spent at least 60 seconds in the track and viewed 2 assets). Not all content tracks will or should have form strategy turned on - it depends on the goal of your campaign. 

**Please Note:** We have [listening campaigns](#listening-campaigns) in Marketo set up to capture consumption of content that would have been gated had PathFactory not been implemented. The listeners also incorporate PathFactory activity into the [MQL scoring model](/handbook/marketing/marketing-operations/marketo/#mql-scoring-model). This means that you do not need to add form strategy to a content track if entry point is from a landing page and there are listening campaigns set up for assets in your track that would normally be gated. 

Form strategy is used on content tracks to collect data from unknown visitors and should only be used when a track entry point is **not** from a webform or landing page (i.e. direct link from digital ad or web promoter). Not all content tracks will or should have form strategy turned on. The forms used in PathFactory are directly tied to currently existing Marketo forms. If the form strategy is implemented, please ensure `Show to Known Visitors` is left **unchecked**. 

**Please Note:** We have [listening campaigns](#listening-campaigns) in Marketo set up to capture consumption of content that would have been gated had PathFactory not been implemented. The listeners also incorporate PathFactory activity into the [MQL scoring model](/handbook/marketing/marketing-operations/marketo/#mql-scoring-model). This means that you do not need to add form strategy to a content track if entry point is from a landing page and there are listening campaigns set up for assets in your track that would normally be gated. 

####### Adding form strategy to a content track

1. In the content track settings sidebar (left), toggle "Forms Strategy" to `On`.
1. Click `View Form Strategy` located below the `Add Content` button.
1. Determine whether the form strategy will be applied to individual assets or the entire track. For individual assets, you'll choose `Content Rules`; for form strategy on the entire content track, you'll choose `Track Rule`.

**Form strategy for individual assets:**

1. Click `Add Rule` in the `Content Rules` row.
1. Select the `General Form (2074) LIVE` form unless otherwise required according to campaign needs.
1. Under `Display Behavior`, click the dropdown and choose the assets where you want the form to show. (**Please Note:** only assets that you have added to the content track will show in the dropdown. If you want the form to show on an asset that _is not_ in the track, you will need to add it first.)
1. Select the amount of seconds you want to delay before the form shows on the asset. Ten seconds is the default selection.
1. Select additional options for the form behavior. If you will be using the content track or individual asset links in an email, you are working with a known audience and therefore should only select `Show to unknown users`. This prevents forms being shown to users who are already known in Marketo. However, if you are using the form on the web or other channels, you'll want to select `Show to unknown users` only.
1. Leave `If submitted, allow form to show again` turned off.
1. You can `allow visitors to dismiss the forms` if it is not crucial to its use to have them submit their info. This decision ultimately lies with the directly responsible MPM.
1. The option `Keep promoters active when form is shown` is also up to the directly responsible MPM. For example, if the `Flow` promoter is used on a content track, they will still be able to see the sidebar of avialable content while the form is shown to them. If this option is turned off, the visitor _will not_ be able to click on any content in the sidebar until they fill out the form.

**Form strategy for content tracks:**

1. Click `Add Rule` in the `Track Rule` row.
1. Select the `General Form (2074) LIVE` form unless otherwise required according to campaign needs.
1. Under `Display Behavior` you can choose to serve the form based on the number of content assets viewed or the total time spent on the track. This decision lies with the directly responsible MPM.
1. All other options for content track rules are the same for individual assets (see above).

##### Testing a track link

1. Click through the experience to ensure assets, CTAs, and forms load properly and that promoters are working as intended.
1. Remove any extraneous `?` (there should only be one immediately after the end of the URL).
1. Watch for extra `&` when appending UTMs.
1. Test and ensure the experience is working as intended.

**When a track is LIVE (in use):**

1. Change the target track title in use to `[LIVE] Name of track`.

**When a track has been archived (no longer in use):**

1. Change the target track title that's no longer in use to `[ARCHIVED] Name of track`.

##### Adjustments to live content tracks

- You can add assets and adjust the position of assets to a `[LIVE]` content track.
- Removing an asset or changing the custom URL slug of an asset in a `[LIVE]` track can disrupt the user experience for the visitor and activate the `#all` track or fallback URL (`about.gitlab.com`) instead of the intended content track. Please ensure that the link to the asset is not being used in any other marketing channels before deleting.

### PathFactory forms

The forms used in PathFactory are hardcoded Marketo form script. They are added to PathFactory using the Marketo script, but they should also include the PathFactory capture tags, Google Tag Manager script to capture form fills in Google Analytics and track form submission back to Marketo, and custom parameters to capture additional information behind the form fill. If a new form is created, the PathFactory capture tag, Google Tag Manager script, and custom paramters **must be hardcoded in the script**. 

### PathFactory links

- Only content track links are meant to be used and shared. Do not share individual asset links from the content library.
- You can use a content track link for multiple use cases as long as you apply UTMs appropriately. Applying UTMs helps us differentiate how the track performed across different channels.
- To ensure proper tracking of an asset in PathFactory, it should be included within a content track and not shared with an individual link from the content library.
- If the link breaks or an asset is deleted, the user will be redirected from your content track to the `#all` track, which includes all assets uploaded to PathFactory. In a case where the user is not redirected to the `#all` track, they will be redirected to the `Fallback URL` which is set to `about.gitlab.com`.

#### Target track links

1. Use the `Get Share URL` feature next to the title of the track. `Share links` are to be used in locations such as the website whereas `Email tracking links` are only for use in email. **Note:** If it’s in email, it’s a known audience so don’t gate any assets in the track. Only use `share links` on the web and those tracks _can have_ gated assets within PathFactory.
1. If you want a particular asset to show first, that asset should be located in the first position of the target track.

#### Recommended track links

1. To use a recommended track link, click on any of the assets in the track and copy the link from the asset window on the right. The asset you choose to share the link will be shown to the user first.

#### Appending UTMs to PathFactory links

1. First check and see if there is a question mark `?` already existing in the PF link. Typically there is one. The only time it won't have a `?` is when you set a custom URL.
1. If there is a question mark `?`, first add an ampersand `&` at the end of the PF link, followed by the UTMs.
    - For example:
    - PF Link: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb`
    - PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?x=53kuPb&utm_source=email&utm_campaign=test`
1. If there is no question mark `?`, first add a question mark `?` at the end of the PF link, followed by the UTMs.
    - PF Link: `https://learn.gitlab.com/c/10-strategies-to-red`
    - PF Link with UTMs: `https://learn.gitlab.com/c/10-strategies-to-red?utm_source=email&utm_campaign=test`

**Marketo links**

1. For a PF Marketo link, it will typically already include a question mark "?". To add UTMs, first add an ampersand `&` at the end of the PF link, followed by the UTMs.
    - Example: `https://learn.gitlab.com/c/devops-explained-git?x=GVFK3F&lb_email={{lead.Email Address}}&utm_source=email&utm_campaign=test`
1. Remove any extraneous `?` (there should only be one immediately after the end of the URL).
1. Watch for extra `&`.
1. Test the link before implementation for quality assurance purposes.

**PathFactory links behind a form fill on a landing page**

1. When using a PathFactory link as the redirect behind a form fill on a landing page, the link format should be as follows:

`https://learn.gitlab.com/c/gcn-dev-sec-ops-how-?x=XOIXTl&lb_email=`

**Using a content track with a custom URL**

1. If your content track has a custom URL, you will notice the `?` in a different location than with content tracks that don't have a custom URL.
    - With no custom URL: `https://learn.gitlab.com/c/gcn-dev-sec-ops-how-?x=XOIXTl&lb_email=` (`x=XOIXTl` = content track ID)
    - With custom URL: `https://learn.gitlab.com/cicd/cloud-ci-tools-matur?lb_email={{lead.Email Address}}`

#### Custom query strings

PathFactory’s custom query string manager allows you to manage and append query strings when sharing a link to a content track or explore page.

1. Select `Organization Settings` in the dropdown menu in the top right corner.
1. Select the `Custom Query Strings` tab.
1. Create your custom query. When you save the new query string, it will be available whenever you access a content track in PathFactory and click the `Share` button or icon.

**Note:** Do not delete or edit the default query string for Marketo as that query string is an important way to integrate with PathFactory.

### Explore pages

Explore pages allow your visitors to quickly view all content assets in a content track. Each explore page you create is built on top of an existing target or recommend content track.

#### Use cases

Explore pages can act as replacements for traditional landing pages or simple microsites.

- Resource center
- Event or webcast follow-up
- Co-branded resource page
- Personalized information hub

#### Create an explore page

**Before you make an explore page ensure that you have already built a content track (target or recommended) that you will use as the base.**

1. Select `Explore` from the left navigation.
1. Click on the `Create Explore Page` button.
1. Enter a name for your explore page and select a content track to use as your base. You can either create an entirely new explore page built on an existing target or recommend content track, or clone an existing explore page.
1. Select which folder the explore page will live in, then click `Create Explore Page`. Please follow the hierarchy of folders by dept. from the content tracks.
1. The colors, fonts, imagery, and layout of your explore page can be customized in `Appearances`. Select your desired appearance from the left navigation in your explore page under `Page Settings`.
1. Under `Page Settings`, ensure that the `Search Engine Directive` is set to `No Index, No Follow`.
1. Choose your desired layout under `Layout Settings`.
1. Choose your desired content settings under `Content Settings`.
1. Ensure that the [gated content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/) in your content track has a [listening campaign](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns).
1. Be sure to test your explore page fully.

#### Explore Page FAQs

**Why can’t I change the background color behind my cover image?**

The cover background color only shows when the cover image is set to "Fixed Width." Because of this, the color selection square is only available when "Fixed Width" has been selected.

**I don’t want this header on my Explore page, how do I get rid of it?**

The header is applied to an Explore experience from the base content track you chose for the explore page. If you don’t want the header on your explore page, go to the base content track and turn the header off.

**I don’t like the order that my content is arranged on the Explore page; how do I move my content around?**

All changes to the content in an explore page has to be done in the base content track that the explore page is built from. This includes the order of your content, so if you want to reorder your content you have to do so in the base content track.

**Can I create a custom hero for a header on my page?**

Yes! You can add a custom hero image by navigating to `Appearances Configuration` and selecting the appearance you would like to modify. Then click on the `Explore` tab and select `cover image` for Hero layout and upoad your custom image under `Hero Image`. Make sure you click `save` on both the explore image upload and then again to save the appearance edits you have made (recommended size for a hero image is 1600x500).

### Appearances

Appearances allow you to control the look and feel of your content tracks. By creating different appearance groups you are able to quickly and easily apply different colors, fonts, and logos to content tracks without having to configure them each time you build a new track. Appearance settings allow you to control how your promoters look, select favicons for your tracks, and configure the appearance of your cookie consent messaging.

You are able to change the appearance of the following components of a content track:

1. Promoters
1. Cookie Consent and Cookie Message
1. Favicon
1. Header

[Video walkthrough of appearances](http://successwith.lookbookhq.com/c/ilos-appearance-conf?x=Blrk3E) (Nook login required)

**Create a new appearance group**

1. Click the gear icon at the top right of the page. Select `Appearances` from the drop-down menu.
1. Click `Add Appearance`.
1. Name the appearance group.
1. Select the color of the text and the primary color by clicking on the colored boxes.
1. Change the font of the text by selecting from the drop-down menu. Click the `B` button to bold the text. Click on the `A` buttons to change the text size.
1. When you are done making changes click `Add Appearance`.
1. Once you’ve created your `Appearance` groups, you can apply this styling to any of your content tracks. Simply select the appearance group from the drop-down menu under `Experience Settings`.

## PathFactory tracking

- [Bizible attribution with PathFactory](https://about.gitlab.com/handbook/marketing/marketing-operations/bizible/#bizible-attribution-with-pathfactory)

### Listening Campaigns

In Marketo, we have programs built to "listen" for PathFactory (PF) activity & content consumption allowing us to track behaviour without having to physically gate all the assets which disrupts the user experience.

The PF<>Marketo listening programs are built to triggered based on the `slug` associated to each of asset. **Very Important: Do NOT to change the `slug` in PF without notifying Ops _prior_ to making the change**. Each listening campaign has a Salesforce (SFDC) campaign associated to it tracking consumption and applying Bizible touchpoints.

The naming convention for each of the listeners is specific to the asset type & is used as a trigger to the appropriate scoring campaign _within Marketo_ at this time these listening campaigns have **no impact** on PathFactory engagement scores. The same naming convention is used for **both** Marketo & Salesforce campaigns.

| **Asset Type** | **Naming Convention** |
| ---------- | ----------------- |
| Analyst report | PF - Analyst report - |
| Assessment | PF - Assessment - |
| Datasheet | PF - Datasheet - |
| Demo | PF - Demo - |
| eBook | PF - eBook - |
| Research report | PF - Research report - |
| Webcast | PF - Webcast - |
| Whitepaper | PF - Whitepaper - |

#### Set up a new listening campaign

This process is for new assets in PathFactory that **have not** already been distributed from a content track. 

**Create Program in Marketo and sync to Salesforce**

1. Navigate to the PathFactory Listening Template - [TEMPLATE - `PF - Asset Type - Name of Asset`](https://app-ab13.marketo.com/#PG3875A1) (located under `Active Marketing Programs` > `Gated Content` > `Pathfactory Listening`)
1. Right-click and clone the template (see picture below) - name it according to the naming convention (example: `PF - Webcast - [campaign_name]`. The campaign name should match the parent campaign for the gated asset for easy searchability.)
1. Move program under the correct folder based on type and the quarter.
1. When the Program Summary loads, next to "Salesforce Campaign Sync" click `not set`
1. From the dropdown, select `Create New` (this will create the synced campaign in Salesforce)

**Update Marketo Smart Campaign**

1. Expand the Marketo program and select `PF - Listening (Triggered)`.
1. In the `Smart List`, add the URL slug you created in the PathFactory content library for your asset within the brackets `[ ]`.
1. In the `Smart List`, add the URL of your landing page to the "not filled out form" filter to make sure you don't create a second touchpoint (a duplicate of the form fill action) upon form fill of your asset.
1. Navigate to the `Schedule` tab and click `Activate`.

![image](/handbook/marketing/marketing-operations/pathfactory/clone-program.png)

**Update the Newly Created Campaign in Salesforce**

1. Navigate to the newly created campaign (same name as you created above in Marketo) - [shortcut to PF Listening Campaigns list](https://gitlab.my.salesforce.com/701?fcf=00B4M000004tY6O&rolodexIndex=-1&page=1)

![image](/handbook/marketing/marketing-operations/pathfactory/sfdc-pf-campaign.png)

1. Check that the `Active` checkbox is checked.
1. Change the Parent Campaign to be the gated asset or on-demand webcast to create a 1:1 relationship.
1. Under Bizible attribution, select `Include only "Responded" Campaign Members` next to `Enable Bizible Touchpoints`.
1. Mark the Status as `In Progress`.
1. Click `Save`.

Assets needing a listening campaign should following the above naming conventions. 

**Important: Please make sure all [gated content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#gating-criteria) in your content track is included in your form strategy and has a [listening campaign](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns). If you are using one of the gated content assets in your content track as the form fill behind a landing page, you need to set up an exclusion in the workflow ([example](https://app-ab13.marketo.com/#SC10049B2)).**

#### Salesforce Campaign Type

For the PathFactory listening campaigns there is a corresponding Salesforce `Campaign Type` to be used. The `Campaign Member Status` simply tracks if the content was consumed. This Salesforce `Campaign Type` should be used for **nothing** else. For greater details, see the [campaigns and programs page](/handbook/marketing/marketing-operations/campaigns-and-programs).

### Custom PathFactory Fields

There are custom PathFactory fields available in Salesforce and Marketo. They are first created in Salesforce and then synced/created in Marketo. 

| **Field Name** | **Purpose** |
| ---------- | ------- |
| PathFactory Asset Type | A tag to help categorize types of content (whitepaper, video, eBook, etc) |
| PathFactory Assets Viewed | Cumulative number of assets viewed. **This is not associated to time consumed! See Content Count for difference** |
| PathFactory Content Count | Cumulative count of assets consumed. Example: if person consumes 2 whitepapers, 1 video and blog post for _minimum of 20 seconds each_ in a **single session** this field would show 4. |
| PathFactory Content Engagement Threshold | The engagement threshold time setting (in seconds) of the content asset. |
| PathFactory Content Journey | Shows users path through a PathFactory experience (next asset viewed from previous asset based on slug) |
| PathFactory Content Language | The language of the content asset. |
| PathFactory Content List | Cumulative list of each assets content id/slug for each asset consumed. |
| PathFactory Content Slug | The custom slug of the content asset. |
| PathFactory Content Source URL | The URL of the underlying source content of the content asset. |
| PathFactory Content Title | The public title of the content asset. |
| PathFactory Content URL | The current public URL of the asset being rendered in the content track. |
| PathFactory Engagement Score | Each asset in content library can be assigned an engagement score; this score is passed from PF to SFDC and used to determine meaningful engagement with content. |
| PathFactory Engagement Time | Cumulative time a person spends consuming assets in session. |
| PathFactory Experience Name | The PathFactory track name - [more details](#content-tracks) |
| PathFactory External ID | A non-unique ID that can be added to tracks &/or assets, which can be leveraged to organize content and configure it in Marketing Automation Platform (i.e Marketo) |
| PathFactory Funnel State | Each asset is tagged with stage of funnel most applicable to asset - Top of Funnel, Middle of Funnel or Bottom of Funnel |
| PathFactory Query String | Returns any values captured by a query string which you have added to the content experience URL. |
| PathFactory Query String Value | The value of a key in the query string. |
| PathFactory Topic List | Assets are tagged by **topic**. This is manually set & aligns with the [tracking content](#tracking-content) list. |
| PathFactory Track Custom URL | The custom URL of the content track. |
| PathFactory Track ID | The automatically generated PathFactory ID of the content track. |

### Industry Verticals

Industry verticals are set using a standard list in SFDC per [this issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2500). 

1. Aerospace & Defense
1. Agriculture
1. Apparel
1. Automotive
1. Banking
1. Biotechnology
1. Chemicals
1. Communications
1. Construction
1. Consulting
1. Education
1. Electronics
1. Energy
1. Engineering
1. Entertainment
1. Environmental
1. Finance
1. Food & Beverage
1. Government
1. Healthcare
1. Hospitality
1. Insurance
1. Local
1. Machinery
1. Manufacturing
1. Media
1. Not For Profit
1. Other
1. Recreation
1. Retail
1. Shipping
1. State
1. Technology
1. Telecommunications
1. Transportation
1. Utilities

### PathFactory Analytics

There are a variety of analytics and reports available in PathFactory that show the overall effectiveness of content. See more info [here](/handbook/marketing/marketing-operations/pathfactory/pathfactory-analytics).