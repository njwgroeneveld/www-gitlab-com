---
layout: markdown_page
title: "Category Direction - Epics"
description: GitLab supports enterprise Agile portfolio and project management frameworks, including Scaled Agile Framework (SAFe), Scrum, and Kanban. Learn more!
canonical_path: "/direction/plan/epics/"
---

- TOC
{:toc}

## Epics

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [Viable](/direction/maturity) |
| Content Last Reviewed | `2020-10-20`                  |


### Overview

#### Purpose

Large enterprises are continually working on increasingly more complex and larger scope initiatives that cut across multiple teams and even departments, spanning months, quarters, and even years. GitLab's vision is to support organizing these initiatives into powerful multi-level work breakdown plans and to enable tracking the execution of them over time, to be extremely simple and insightful. In addition, GitLab should highlight which opportunities have higher ROI, helping teams make strategic business planning decisions.

GitLab supports popular [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/),
including Scaled Agile Framework [(SAFe)](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/), [Scrum, and Kanban](https://about.gitlab.com/solutions/project-management/).

![epicstree-direction.png](/direction/plan/epics/epicstree-direction.png)

#### Top Strategy Item(s)

To drive our vison for Epics and how they supports our overall strategy for Product Planning we are continuing to develop the building blocks for deeper more expansive program and portfolio functionality. Currently we are validating and tracking the following big ticket items :

1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)
1. [Health/Risk Reporting](https://gitlab.com/gitlab-org/gitlab/issues/202420)
1. [Epic Dependency Mapping](https://gitlab.com/groups/gitlab-org/-/epics/2581)
1. [Tracking Epic Progress](https://gitlab.com/groups/gitlab-org/-/epics/76)


### What's next & why

Now that our users can properly build out epics as a collection of associated issues, we are focusing on developing advanced functionality to make Epics more poweful planning artifacts. To begin, we will be pursuing multiple enhancements to the current Epic planning artifcat and how it relates to Issues, Milestones, and our Roadmaps: 

1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/-/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)

GitLab's multi-level work breakdown planning capabilities will include [multiple layers of epics](https://gitlab.com/groups/gitlab-org/-/epics/312)
and [issues](https://gitlab.com/groups/gitlab-org/-/epics/316), allowing enterprises to capture and manage:

- Portfolios, programs, and projects across their organization via [Groups](https://docs.gitlab.com/ee/user/group/)
- High-level [strategic initiatives and OKRs (objectives and key results)](https://gitlab.com/gitlab-org/gitlab/issues/36775).
- Dependency management both for [epics](https://gitlab.com/groups/gitlab-org/-/epics/2581) and [issues](https://gitlab.com/groups/gitlab-org/-/epics/2032).
- [Project Health/Risk](https://gitlab.com/groups/gitlab-org/-/epics/2204) 

### Maturity Plan

This category is currently at the **viable** level, and our next maturity target is **complete** by 2021-01-31.

We are tracking progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/967).

We are also tracking our progress toward **lovable** via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/968).

### Competitive landscape

Leveraging Epics as a building block for Product and Portfolio Management is a common use case in the industry, with many are established players such as Jira, Clarity, Planview, VersionOne, AgileCraft, and ServiceNow offering similiar functionality. Many of these tools were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools typically have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. Our strategy is building _toward_ those enterprise use cases, starting with the product-development baseline abstractions. 


### Top User Issue(s)

As the usage of Epics increase and more use cases and needs are being surfaced, we are tracking our most popular issues in the category. In particular, we are focusing on:

1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)
1. [Project Level Epics](https://gitlab.com/gitlab-org/gitlab/issues/31840)
1. [Tracking Epic Progress](https://gitlab.com/groups/gitlab-org/-/epics/76)

### Top Customer Success/Sales Issue(s)

To support our Customer Success and Sales departments, we are validating and working towards critical items to enable them to serve additional prospects and customers:

1. [Provide an Epic Swimlane on a Board](https://gitlab.com/gitlab-org/gitlab/issues/7371)
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)
1. [Tracking Epic Progress](https://gitlab.com/groups/gitlab-org/-/epics/76)

