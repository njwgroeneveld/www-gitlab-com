---
layout: markdown_page
title: "Product Section Direction - Dev"
description: "The Dev Section is made up of the Manage, Plan, and Create stages of the DevOps lifecycle and encompasses a number of analyst categories"
canonical_path: "/direction/dev/"
---

- TOC
{:toc}

Last Reviewed: 2020-11-11

<%= devops_diagram(["Manage","Plan","Create"]) %>

Our latest group conversation from Sept 15, 2020:

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/sh0zrGZDbIY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Dev Section Overview

The Dev Section is made up of the [Manage](/handbook/product/product-categories/#manage-stage), [Plan](/handbook/product/product-categories/#plan-stage), and [Create](/handbook/product/product-categories/#create-stage) stages of the DevOps lifecycle. The scope for the Dev section is wide and encompasses a number of analyst categories including Value Stream Management, Project Portfolio Management, Enterprise Agile Planning Tools, Source Code Management, IDEs, Design Management, and even ITSM. It is difficult to truly estimate TAM for the Dev Section, as our scope includes so many components from various industries, but research from IDC indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1HYi_l8v-wTE5-BUq_U29mm5aWNxnqjv5vltXdT4XllU/edit?usp=sharing) in 2019 is roughly ~$3B, growing to ~$7.5B in 2023 (26.5% CAGR). Alternatively, the Dev product management team has conducted a bottoms up Total Addressable and Servicable Addressible market analysis which estimates GitLab's SAM for Dev to be 19.5B in 2019 growing to 27.5B in 2023. Analysis: [Manage](https://docs.google.com/spreadsheets/d/1357Zwbf0yTjcFBuCdX2HnNLBaF9uWnW7Auv4F94aoSo/edit#gid=0) [Plan](https://docs.google.com/spreadsheets/d/1dDNSR_mE4peeOc_3Xhqm_VLoTyk4gSbcbiGfuMrOg6g/edit#gid=0) [Create](https://docs.google.com/spreadsheets/d/1KSwTVPIvMO8IXkMqA40Ms1rL-glmo-w7MjZvUrjMFGA/edit#gid=0). NOTE: The large disparity in the TAM/SAM approaches is due to percentage attribution of DevOps revenue. For example, in the IDC report, it specified a certain percentage of Atlassian revenue for DevOps, but in the bottoms up analysis most of Atlassian revenue would have been counted since we looked at all project/portfolio management revenue.

Based on [DevOps tool revenue](https://drive.google.com/file/d/1bd4kj1M6iTebdqguFDkTC363TNCDnILa/view) at the end of 2019 and comparing to GitLab annual recurring revenue at the end of FY21 Q3, our estimated market share is approximately 1.8% based on revenue. (Note: this assumes we can attribute 100% of GitLab revenue to Dev stages.) Market share based on source code management is somewhere in the [35%](https://about.gitlab.com/is-it-any-good/#gitlab-is-the-most-popular-scm-tool-among-jvm-devs) range.

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) still have not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern, Git-backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry-leading solutions in source code and code review, as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform. Once a user has begun utilizing repositories and code review features like Merge Requests, they often move “left” and “right” to explore and utilize other capabilities in GitLab, such as CI and project management features.

Per our [Stage Monthly Active User data](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/) we know that Create and Plan have the highest usage amongst GitLab stages. As such, these stages must focus on security fixes, bug fixes, performance improvements, UX improvements, and depth more than other areas of GitLab.

Other areas, such as Value Stream Management are nascent to both GitLab and the market, and will require more time devoted to executing problem and solution validation discovery. 

Within each stage, the listed items in the FY21 plan are in **order of priority**. The top priority for each stage is:

* Manage: [Enterprise Readiness](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Enterprise%20Readiness)
* Plan: [Epic Swimlanes](https://gitlab.com/groups/gitlab-org/-/epics/3352)
* Create: [Gitaly Cluster](https://gitlab.com/groups/gitlab-org/-/epics/1489)
* Ecosystem: [Atlassian Integration](https://gitlab.com/gitlab-org/gitlab/-/boards/1789772?label_name[]=atlassian)

### Is Our Dev Strategy Any Good?

In December of 2019, we released a [blog](https://about.gitlab.com/blog/2019/12/04/dev-strategy-review/) with most of the content on this page and asked readers of the blog for feedback. To solicit the feedback, we asked our users on a scale from 1-5 how likely they were to recommend GitLab to a friend or colleague based on the strategy and the primary reason. 84% of survey particpants responded with a 4 or 5 vote. Here are the full results:

![Survey Results](/images/direction/dev/survey-results.png)

The primary reasons for the numbered score above were:
* GitLab is an entire DevOps tool
* Placing the strategy portion into GitLab will help ensure that features are tracked back to strategy. Connect the loop!
* The quality and well thought out design of GitLab makes its adoption a no-brainer
* Plan addresses the most important aspects
* Using GitLab to make the mindset shift to holistic product management
* GitLab is good, but we still see regressions negatively affecting our workflow at least a few times every year
* GitLab seems to “know the pulse” of their users / customers and, therefore, are focusing plans on delivering those features / functionality that best impact their experiences with the solution.
* I like the core features of GitLab and GitLab CI. I don't care about any planning, product management tracking, ml, etc features.
* I'm already very satisfied with Gitlab in general. But please, please invest even more in your Wiki system. For most companies it's an integral part of the documentation of a software. Generating a TOC sidebar based an the current directory/page would be awesome!
* Better Issue-Tracking (Jira...), Code-review
* Focus on improving code review
* Clear, and achievable, goals. Open communication.
* GitLab is focused to be practical
* New useful features gets implemented on regular basis but many get abandoned after the first iteration (see for examples "release").
* Love the E2E thinking when talking about product and dev. Visualizing business value and impact, tracking progress and understanding ROI are top challenges to be addressed, at least for me as engineering leader.
* GitLab is great, but the tiers and the pricing of them is not.
* You adopt a customer-centric approach. For me it means even if some initiative does not succeed in a first attempt, transparency and dialog help success to happen in the long-term.

## Who is it for?
We identify the [personas](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) the Dev section features are built for. In order to be transparent about personas we support today and personas we aim to support in the future we use the following categorization of personas listed in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Dev section has features that make it useful to the following personas today.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Parker - Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟨 [Presley - Product Designer](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟨 [Cameron - Compliance Manager](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. 🟨 [Rachel - Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Simone - Software Engineer in Test](/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Allison - Application Ops](/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. 🟨 [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Sidney - Systems Administrator](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

### Medium Term (1-2 years)
As we execute our [3-year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single application that enables collaboration between cloud native development and platform teams.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Parker - Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟩 [Presley - Product Designer](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟩 [Cameron - Compliance Manager](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. 🟩 [Simone - Software Engineer in Test](/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Rachel - Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Allison - Application Ops](/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. 🟨 [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Sidney - Systems Administrator](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

## 3 Year Strategy

In three years, the Dev Section market will:

* Centralize around Git as the version control of choice for not only code, but for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently, the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management.
* Recognize the value of a single platform for all software creation activities, including product management.
* See an uptick in startups, applications, and internal business logic being built on the backs of low or no-code platforms. We recently merged in a low/no-code [direction]((/direction/nolowcode) page if you'd like more information on our direction in this space.
* Related to low/no code frameworks mentioned above, the market will see an increase in apps built on AI technology. In June of 2020, [GPT-3](https://en.wikipedia.org/wiki/GPT-3) was released and early adopters shared incredible use cases based on this technology. Within 3 years, it's likely this technology has advanced significantly and is starting to disrupt modern software development.
* Coalesce around the notion of a single platform for DevOps, also known as Value Stream Delivery Platforms.

As a result, in three years, GitLab will:

* Provide a next-generation, highly performant Git-backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to, as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.
* Develop an industry-leading product management platform where multiple features and products can be measured and managed easily.
* Research and potentially add capabilities for "no code" workflows and GPT-3 developed applications.
* Ensure we are the best VSDP on the market.

## 3 Year Themes

Our direction for the Dev section is to provide the world’s best product creation and management platform.
We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by
providng a solution that breaks down organizational silos and enables maximum value delivery.
We want to provide a solution that enables higher-quality products to be quickly iterated upon. We also want to make it effortless for companies to migrate to GitLab. In order to obtain adoption at scale, GitLab has to provide *substantially more value*
than our competitors. Additionally, we believe the majority of value can likely be delivered by substantially fewer
features than our competition. The following themes listed below represent how we believe we will deliver this value
and is our view of what will be important to the market and to GitLab over the next 3 to 5 years. As such,
they will be the cornerstone of our 3-year strategy, and all activities in the 1-year plan should advance
GitLab in one or more of these areas.

<%= partial("direction/dev/themes/automated_code_review") %>

<%= partial("direction/dev/themes/value_stream_measurement") %>

<%= partial("direction/dev/themes/more_devops_personas") %>

<%= partial("direction/dev/themes/enterprise_digital_transformation") %>

<%= partial("direction/dev/themes/project_to_product") %>

<%= partial("direction/dev/themes/remote_development") %>

## 1 Year Plan: What’s Next for Dev

Please see the [categories page](/handbook/product/product-categories/#dev-section) for a more detailed look at Dev's plan by exploring `Direction` links in areas of interest. This page will highight direction themes for both one year and three year timelines.

<%= partial("direction/dev/plans/manage") %>

<%= partial("direction/dev/plans/plan") %>

### Create

<%= partial("direction/dev/plans/create") %>

### Themes that cross all Dev stages
**Performance and availability:** We must invest in the performance, stability, and availability of our application. We will do this by focusing on [application limits](https://gitlab.com/groups/gitlab-org/-/epics/1737#note_202179305), [diff load times](https://gitlab.com/groups/gitlab-org/-/epics/1816), and ensuring [availability](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work) is top of mind.

Growth driver: Retention

### What we're not currently focusing on

Choosing to invest in the above areas in 2020 means we will choose not to:
* Efficiency recommendations: Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. We won't be working on features that help companies answer, “Am I doing the right activities?” These improvements will come in years two and three of the VSM plan.
* Container based IDEs or remove dev environments. We recently merged in a community contribution from GitPod, which provides this functionality and will understand the usage of this feature before making large investments here.
* Feature objects: Creating first class feature objects per the project management morphs into product management block below. While we think this is important, we must spend this year building a better foundation in portfolio management by making our groups/projects structure more simple and delivering epic boards.
* Wikis: Investments in wikis such as WYSIWYG editing or real time collaboration ala Notion.

## Stages & Categories

<%= partial("direction/dev/strategies/manage") %>

<%= partial("direction/dev/strategies/plan") %>

<%= partial("direction/dev/strategies/create") %>

## What's Next 

<%= direction["all"]["all"] %>
