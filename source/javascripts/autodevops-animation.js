(function(){

  // Timeline animation

  var loadingBarEasing = "M0,0 C0,0 0.061,0 0.076,0 0.077,0.016 0.138,0.083 0.153,0.083 0.153,0.1 0.154,0.149 0.155,0.166 0.17,0.166 0.214,0.166 0.23,0.166 0.23,0.183 0.444,0.416 0.46,0.416 0.46,0.433 0.461,0.595 0.461,0.612 0.477,0.612 0.629,0.619 0.645,0.619 0.645,0.636 0.646,0.685 0.646,0.702 0.661,0.702 0.707,0.702 0.722,0.702 0.723,0.719 0.906,0.916 0.921,0.916 0.922,0.933 0.922,0.983 0.923,1 0.938,1 1,1 1,1"
  var moonPath = "M11.3333333,23.938172 C11.3333333,14.6599462 16.6687179,6.63978495 24.4364103,2.78696237 C25.6917949,2.15793011 25.2210256,0.349462366 23.8871795,0.349462366 C22.945641,0.349462366 22.0825641,0.428091398 21.1410256,0.50672043 C9.68564103,1.92204301 0.819487179,11.5147849 0.348717949,22.9946237 C-0.200512821,36.4401882 10.5487179,47.5268817 23.8871795,47.5268817 C23.8871795,47.5268817 23.8871795,47.5268817 23.965641,47.5268817 C25.2994872,47.5268817 25.6917949,45.718414 24.5148718,45.0893817 C16.6687179,41.2365591 11.3333333,33.2163978 11.3333333,23.938172 Z";

  var blockEasing = 0.4;
  var blockDelay = 0.2;

  var autoDevopsStepEasing = 0.4;
  var autoDevopsStepDelay = 0;

  var totalAnimationTime;

  var developerRightShirt = document.getElementById('developer-right-shirt')
  var developerLeftShirt = document.getElementById('developer-left-shirt')

  function leftAnimation() {

    var tlLeft = new TimelineLite();
    // var tl = new TimelineLite({onComplete:function() {
    //   this.restart()}
    // });

    // Processes

    // Code it up
    tlLeft.fromTo('.container-left #code-it-up', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1, delay: .6});
    tlLeft.staggerFromTo('.container-left #code-it-up .code-lines path', .4, {scale:(1,0)}, {scale:(1,1)}, -0.3);
    tlLeft.to('.container-left #code-it-up', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // Gerrit review
    tlLeft.fromTo('.container-left #gerrit-review', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.staggerTo('.container-left #gerrit-review .code-lines path', .4, {ease: Power4.easeOut, opacity: 1}, -0.3);
    tlLeft.staggerTo('.container-left #gerrit-review .code-lines path', .4, {ease: Power4.easeOut, opacity: .2}, -0.3, '-=2.8');
    tlLeft.to('.container-left #gerrit-review .code-lines path', .4, {ease: Power4.easeOut, opacity: 1});
    tlLeft.to('.container-left #gerrit-review', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // // Github SCM
    tlLeft.fromTo('.container-left #github-scm', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.staggerTo('.container-left #github-scm .code-lines path', .6, {ease: Power2.easeIn, x:300}, 0.1);
    tlLeft.to('.container-left #github-scm', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // Jenkins build
    tlLeft.fromTo('.container-left #jenkins-build', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.staggerFromTo('.container-left #jenkins-build .code-lines path', .6, {ease: Power4.easeOut, y:300}, {ease: Power4.easeOut, y:0}, -0.3);
    tlLeft.to('.container-left #jenkins-build', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // Jenkins test
    tlLeft.fromTo('.container-left #jenkins-test', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test .code-lines', .1, {ease: Power4.easeOut, y: 22}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10});
    tlLeft.to('.container-left #jenkins-test .code-lines', .1, {ease: Power4.easeOut, y: 10}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test', .2, {ease: Power4.easeOut, y: 0});
    tlLeft.to('.container-left #jenkins-test .code-lines', .2, {ease: Power4.easeOut, y: 15.39}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10, delay: .6});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10});
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test .code-lines', .1, {ease: Power4.easeOut, y: 22}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: -10});
    tlLeft.to('.container-left #jenkins-test .code-lines', .1, {ease: Power4.easeOut, y: 10}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', .1, {ease: Power4.easeOut, y: 10});
    tlLeft.to('.container-left #jenkins-test', .2, {ease: Power4.easeOut, y: 0});
    tlLeft.to('.container-left #jenkins-test .code-lines', .2, {ease: Power4.easeOut, y: 15.39}, '-=.1');
    tlLeft.to('.container-left #jenkins-test', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // // Black duck scan
    tlLeft.fromTo('.container-left #black-duck-scan', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.to('.container-left #black-duck-scan #magnifying-body', 1, {ease: Power1.easeOut, x: 140});
    tlLeft.to('.container-left #black-duck-scan #magnifying-mask', 1, {ease: Power1.easeOut, x: 140}, '-=1');
    tlLeft.to('.container-left #black-duck-scan #mask-div', 1, {ease: Power1.easeOut, x: 140}, '-=1');
    tlLeft.to('.container-left #black-duck-scan', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // // Binary stored
    tlLeft.fromTo('.container-left #binary-stored', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.to('.container-left #binary-stored .binary-text', .6, {ease: Power4.easeOut, opacity: 0});
    tlLeft.to('.container-left #binary-stored .load-bar', .8, {ease: Power4.easeOut, width:100});
    tlLeft.to('.container-left #binary-stored .load-bar', 1.2, {ease: Power4.easeOut, width:133});

    // Guy gets frustrated
    tlLeft.to('.container-left #eyebrow-left', .2, {ease: Power4.easeOut, rotation: 10});
    tlLeft.to('.container-left #eyebrow-right', .2, {ease: Power4.easeOut, rotation: -10}, '-=.2');
    tlLeft.to('.container-left .head-container', .3, {ease: Power4.easeOut, y: 4}, '-=.4');
    tlLeft.staggerTo('.container-left .swear', .2, {ease: Power4.easeOut, opacity: 1}, -0.1, '-=.7');
    tlLeft.staggerTo('.container-left .swear', .2, {ease: Power4.easeOut, opacity: 0}, -0.1);


    tlLeft.to('.container-left #binary-stored .load-bar', .8, {ease: Power4.easeOut, width:162, delay: 1});
    tlLeft.to('.container-left #binary-stored', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // // Chef deploy
    tlLeft.fromTo('.container-left #chef-deploy', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.to('.container-left .chef-binary', 2, {ease: Power0.easeNone, x: 800});
    tlLeft.to('.container-left .chef-container', .4, {ease: Power4.easeOut, x: -86});
    tlLeft.staggerTo('.container-left .chef-container polygon', .4, {ease: Power4.easeOut, opacity: .2}, 0.1);
    tlLeft.staggerTo('.container-left .chef-container polygon', .4, {ease: Power4.easeOut, opacity: 1}, 0.1, '-=.2');
    tlLeft.to('.container-left #chef-deploy', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    // Automated tests
    tlLeft.fromTo('.container-left #automated-tests', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.staggerTo('.container-left #automated-tests .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, 1.2);
    tlLeft.staggerTo('.container-left #automated-tests .check', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, 1.2, '-=3.6');
    tlLeft.to('.container-left #automated-tests', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    // Manual tests
    // tlLeft.fromTo('.container-left #manual-tests', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    // tlLeft.staggerTo('.container-left #manual-tests .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, 1.2);
    // tlLeft.staggerTo('.container-left #manual-tests .check', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, 1.2, '-=3.6');
    // tlLeft.to('.container-left #manual-tests', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    // Approval
    tlLeft.fromTo('.container-left #approval', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlLeft.to('.container-left #approval', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: 2});
    tlLeft.staggerTo('.container-left .swear', .2, {ease: Power4.easeOut, opacity: 1}, -0.1, '-=.7');
    tlLeft.staggerTo('.container-left .swear', .2, {ease: Power4.easeOut, opacity: 0}, -0.1);
    //
    // // Chef production
    tlLeft.fromTo('.container-left #chef-production', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1}, '-=.2');
    tlLeft.staggerFromTo('.container-left #chef-production .code-lines path', .6, {ease: Power4.easeIn, y:0}, {ease: Power4.easeIn, y:-80}, -0.1);
    tlLeft.to('.container-left #chef-production', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    tlLeft.to('.container-left #eyebrow-left', .2, {ease: Power4.easeOut, rotation: 0});
    tlLeft.to('.container-left #eyebrow-right', .2, {ease: Power4.easeOut, rotation: 0}, '-=.2');
    tlLeft.staggerTo('.container-left .head-container', .3, {ease: Power4.easeOut, y: 0}, '-=.4');


    tlLeft.play();

    totalAnimationTime = tlLeft.totalDuration()
    currentDuration = tlLeft.duration()

    TweenMax.to('.container-left .sun-and-moon', totalAnimationTime, {ease: Power0.easeNone, x: -500});

    // when timeline is half finished, darken background and morph sun
    TweenMax.delayedCall(totalAnimationTime / 2, function() {
      TweenMax.to('.container-left .background', 1, {ease: Power4.easeOut, fill:"#ECE8F6"});
      TweenMax.to(".container-left .sun-path", 1, {ease: Power4.easeOut, morphSVG:moonPath});
      TweenMax.to('.container-left .sun-path', 1, {ease: Power4.easeOut, x:-40});
    });

    developerLeftShirt.classList.add('shirt-animation')

    rightAnimation(totalAnimationTime)

  }

  function rightAnimation(totalAnimationTime, currentDuration) {

    var tlRight = new TimelineLite();
    // var tl = new TimelineLite({onComplete:function() {
    //   this.restart()}
    // });

    // Processes

    // Code it up
    tlRight.fromTo('.container-right #code-it-up', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1, delay: .6});
    tlRight.staggerFromTo('.container-right #code-it-up .code-lines path', .4, {scale:(1,0)}, {scale:(1,1)}, -0.3);
    tlRight.to('.container-right #code-it-up', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    // Commit to GitLab
    tlRight.fromTo('.container-right #commit-gitlab', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    tlRight.staggerTo('.container-right #commit-gitlab .code-lines path', .6, {ease: Power2.easeIn, x:300}, 0.1);
    tlRight.to('.container-right #commit-gitlab', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});
    //
    // // Autodevops
    tlRight.fromTo('.container-right #auto-devops', blockEasing, {ease: Power4.easeOut, y: 20, opacity: 0}, {ease: Power4.easeOut, y: 0, opacity: 1});
    //
    // //// Autodevops steps

    tlRight.call(function() {
      developerRightShirt.classList.remove('shirt-animation')
    });
    // Character movements
    tlRight.to('.container-right .eyebrows', .3, {ease: Power4.easeOut, y: 36});
    tlRight.to('.container-right .head-container', .3, {ease: Power4.easeOut, y: 4}, '-=.3');
    tlRight.to('.container-right .mouth', .2, {ease: Power4.easeOut, strokeDashoffset: 0});


    // Build
    tlRight.fromTo('.container-right .code-lines-container #build', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1}, '-=2');
    tlRight.to('.container-right .code-lines-container #build .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.6');
    tlRight.to('.container-right .code-lines-container #build polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #build', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});

    // Test
    tlRight.fromTo('.container-right .code-lines-container #test', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1});
    tlRight.to('.container-right .code-lines-container #test .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.3');
    tlRight.to('.container-right .code-lines-container #test polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #test', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});
    //
    // Scan
    tlRight.fromTo('.container-right .code-lines-container #scan', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1});
    tlRight.to('.container-right .code-lines-container #scan .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.3');
    tlRight.to('.container-right .code-lines-container #scan polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #scan', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});

    // Store
    tlRight.fromTo('.container-right .code-lines-container #store', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1});
    tlRight.to('.container-right .code-lines-container #store .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.3');
    tlRight.to('.container-right .code-lines-container #store polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #store', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});

    // Deploy to stage
    tlRight.fromTo('.container-right .code-lines-container #deploy-stage', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1});
    tlRight.to('.container-right .code-lines-container #deploy-stage .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.3');
    tlRight.to('.container-right .code-lines-container #deploy-stage polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #deploy-stage', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});

    // Approved
    tlRight.fromTo('.container-right .code-lines-container #approved', autoDevopsStepEasing, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 18, opacity: 1});
    tlRight.to('.container-right .code-lines-container #approved .approval-circle-shape', .6, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.3');
    tlRight.to('.container-right .code-lines-container #approved polyline', .4, {ease: Power4.easeOut, strokeDashoffset: 0}, '-=.1');
    tlRight.to('.container-right .code-lines-container #approved', autoDevopsStepEasing, {ease: Power4.easeOut, y: 8, opacity: 0, delay: autoDevopsStepDelay});

    // Production
    tlRight.fromTo('.container-right .code-lines-container #deploy-production', .4, {ease: Power4.easeOut, y: 30, opacity: 0}, {ease: Power4.easeOut, y: 16, opacity: 1});
    tlRight.fromTo('.container-right .code-lines-container #deploy-production .mouse', .8, {ease: Power4.easeOut, y: 40, x: 40, opacity: 0}, {ease: Power4.easeOut, y: 0, x: 0, opacity: 1});
    tlRight.to('.container-right .code-lines-container #deploy-production .mouse', .1, {ease: Power4.easeOut, scale: .8});
    tlRight.to('.container-right .code-lines-container .deploy-button', .1, {ease: Power4.easeOut, scale: .95}, '-=.1');
    tlRight.to('.container-right .code-lines-container #deploy-production .mouse', .1, {ease: Power4.easeOut, scale: 1});
    tlRight.to('.container-right .code-lines-container .deploy-button', .1, {ease: Power4.easeOut, scale: 1}, '-=.1');

    // End gitlab
    tlRight.to('.container-right #auto-devops', blockEasing, {ease: Power4.easeOut, y: -20, opacity: 0, delay: blockDelay});

    // Character movements
    tlRight.to('.container-right .head-container', .3, {ease: Power4.easeOut, y: 0}, '-=.3');
    tlRight.to('.container-right #arm-left', .3, {ease: Power4.easeOut, x: -14, y: 4});
    tlRight.to('.container-right #arm-right', .3, {ease: Power4.easeOut, x: 66, y: 6});
    tlRight.to('.container-right .body-container', .3, {ease: Power4.easeOut, rotation: 6}, '-=1.2');
    tlRight.to('.container-right .arms', .3, {ease: Power4.easeOut, y: 50, opacity: 1});
    tlRight.to('.container-right .mouth', .3, {ease: Power4.easeOut, strokeDashoffset: -12, strokeWidth: 5});
    tlRight.staggerTo('.container-right .whistle-note', 3, {ease: Power0.easeNone, x: -50, y: -50, opacity: 1, repeat:-1}, 1, '-=.3');


    tlRight.play();

    TweenMax.to('.container-right .sun-and-moon', totalAnimationTime, {ease: Power0.easeNone, x: -500});

    // when timeline is half finished, darken background and morph sun
    TweenMax.delayedCall(totalAnimationTime / 2, function() {
      TweenMax.to('.container-right .music-notes-container', 1, {ease: Power0.easeNone, stroke:"#FFFFFF"});
      TweenMax.to('.container-right .background', 1, {ease: Power4.easeOut, fill:"#ECE8F6"});
      TweenMax.to(".container-right .sun-path", 1, {ease: Power4.easeOut, morphSVG:moonPath});
      TweenMax.to('.container-right .sun-path', 1, {ease: Power4.easeOut, x:-40});
    });

    developerRightShirt.classList.add('shirt-animation')

  }

  leftAnimation();

})();
